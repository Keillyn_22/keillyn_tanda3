import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
margin-top: 20px;
margin-left: 20px;
width: 400px;
display: flex;
align-items: center;
flex-direction: column;
`

const Title = styled.p`
color: #626060;
text-transform: uppercase;
font-size: 15px;
font-weight: bold;
width: 300px;
`

const Icon = styled.img`
height: 145px;
`

const App = (params) => {
	return(
    <Container>
    	<Title>Proteins</Title>
          <Icon src={params.icon} />
    	<Title>whey proteins </Title>
    </Container>
		);
}

export default App;