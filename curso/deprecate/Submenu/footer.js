import React from 'react';
import styled from 'styled-components';
import { Circulo, Icon} from './options.js';
import Check from '../images/check-k.png';
import cancelar from '../images/cancel.png';
import reset from '../images/reset.png';

const Containter = styled.div`
display: flex;
justify-content: center;
align-items: center;
flex-direction: row;
`;

const ContainterV1 = styled.div`
display: flex;
justify-content: center;
align-items: center;
flex-direction: row;
margin-top: 15px;
padding-top: 25px;
padding-bottom: 25px;
border-Top: 1px solid #ffffff80;
`;

  const Item = ({ v, onClick }) => {
        return(
          <Circulo visible={true} style={{marginLeft: 15, height: 60, width: 60}} onClick={onClick}>
             <Icon visible={true} src={v} style={{height: 60, width: 60}}/>
          </Circulo>
        )
    }

const App = ({ version, onReset, OnCancel, onAcept }) => {

    const handleReset = () => {
      
        console.log("Vamos a resetear");

        if (typeof onReset === 'function') onReset() 
    }

     const handleOk = () => {
       
        console.log("Vamos a aceptar");

        if (typeof onReset === 'function') onAcept() 
    }
 
      const handleCancel = () => {
      
        console.log("Vamos a cancelar");

        if (typeof onReset === 'function') OnCancel() 
     }

   if (version === 1) {

    let data = [
    {icon: cancelar, onClick: handleCancel },
    {icon: reset, onClick: handleReset },
    {icon: Check, onClick: handleOk }
    ];

       return(
       
       <ContainterV1>

        {data.map((v, i) => <Item v={v.icon} onClick={v.onClick} />)}     

       </ContainterV1>
      );

    }

    if (version === 2) {

         let data = [
    {icon: cancelar, onClick: handleCancel },
    {icon: Check, onClick: handleOk }
    ];
        
        return(
       
       <Containter>

       {data.map((v, i) => <Item v={v.icon} onClick={v.onClick} />)}  

       </Containter>
      
      );
    }

     return <></>
};

export default App;


