import styled from "styled-components";

const Container = styled.div`
    display: flex;
    flex-direction: column;
`; 

const Img = styled.img`
    width: 100%;
    object-fit: cover;
`;

const Body = styled.div`
    display: flex;
    flex-direction: column;
    padding-left: 10px;
    padding-right: 10px;
`;

const Title = styled.p`

`;

const Description = styled.p`

`;

const defaultProps = {
	/*title: "Tienda 1"
	image: "fondocruzroja.png"*/
}

export interface params {
    image: string
    title: string
    description?: string
}

const App = (params: params): JSX.Element => {

    params = { ...defaultProps, ...params }

	return(

		<Container>

		<Img src={params.image} />

        <Body>
           
           <Title> {params.title} </Title>

           <Description> {params.description} </Description>
        
        </Body>
			
		</Container>

		)
	}

export default App 
