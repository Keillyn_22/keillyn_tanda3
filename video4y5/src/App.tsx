import { useState } from "react"

import Card, { params as paramsCard} from "./components/Card"

import CardProduct from "./components/Product"

import Grid from "./components/Grid"

import Telefono from "../public/telefonoM.png"

const App = (): JSX.Element => {

      const [data, setData] = useState<paramsCard[]>([
           { value: "1", image: "/vite.svg", title: "Tienda 1"},
           { value : "2", image: "/vite.svg", title: "Tienda 2"},
           { value: "3", image: "/vite.svg", title: "Tienda 3"},
           { value: "4", image: "/vite.svg", title: "Tienda 4"},
           { value: "5", image: "/vite.svg", title: "Tienda 5"},
           { value: "6", image: "/vite.svg", title: "Tienda 6"},
           { value: "7", image: "/vite.svg", title: "Tienda 7"},
           { value: "8", image: "/vite.svg", title: "Tienda 8"},
        ])

      const handleSelect: paramsCard['onClick'] = (e) => {
        console.log(e)
      }

  return (
       <Grid> 

            <CardProduct image={Telefono} title="telefono" />

            {data.map((v, i) => <Card key={i} {...v} onClick={handleSelect}/>)}
       
       </Grid>
    )
}

export default App
