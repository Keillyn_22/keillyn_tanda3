import styled from "styled-components";
import Checkbox from './checkbox.tsx';
import Check from '../images/check-k.png';

const Containter = styled.div`
margin-left: 25px;
margin-top: 15px;
display: flex;
flex-direction: row;
justify-content: space-between;
margin-right: 25px;
align-items: center;
`;

const Title = styled.p`
margin: 0px;
color: white;
font-weight: 500;
`;

export interface params {
   value: string;
   onClick?: (params: { value: string, state: boolean }) => void;
};


const  App = (params: params): JSX.Element => {

   const handleClick = (state: boolean) => {

      if (typeof params.onClick === 'function') {
         params.onClick({ value: params.value, state });
      };

   };

	return (
         <div>
         	
         <Containter>
       	
       	    <Title>
       		    {params.value}
       	    </Title>
         
            <Checkbox image={Check} onClick={handleClick} />

         </Containter>

         </div>

	);
};

export default App;