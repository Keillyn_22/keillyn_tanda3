import React, {useState} from 'react';
import styled from 'styled-components';
import Card from './Card'
import Submenu from './Submenu/';
import Prote from './Proteina.js';
import Pro from './images/ver.png';

const Containter = styled.div`
`;


const App = () => {

  const [data, setData] = useState([
{
   id: 1,
   color: "#21d0d0",
   options: [ 
    {title: "PRINCE LOW TO HIGH", state: false},
    {title: "PRINCE HIGH TO LOW", state: false},
    {title: "POPULARITY", state: true},

    ],
     footer: 2
  },
  {
   id: 2,
   color: "#ff7745",
   options: [ 
    {title: "1UP NUTRIRION", state: false},
    {title: "ASITIS", state: false},
    {title: "AVVATAR", state: false},
    {title: "BIG MUSCLES", state: false},
    {title: "BPI SPORTS", state: false},
    {title: "BSN", state: false},
    {title: "CELLUCOR", state: false},
    {title: "DOMIN8R", state: false},
    {title: "DIMATIZE", state: false},
    {title: "DINAMIK", state: false},
    {title: "ESN", state: false},
    {title: "EVOLUTION NUTRITION", state: false},

    ],
    footer: 1
  }
])

 const handleChange = (value, id) => {

 const match = data.filter(el => el.id === id)[0];

 const act = match.options.filter(el => el.title === value)[0];

 const copy = [...match.options];

  //find index of item to be replaced
  const targetIndex = copy.findIndex(f => f.title === value);

  if (targetIndex > -1) {
    //replace the object  with a new one.
    copy[targetIndex] = { title: value, state: !act.state};

    //setData(copy)
    match.options = copy;
  };

  const copyList = [...data];

  const targetIndexList = copyList.findIndex(f => f.title === id);

  if (targetIndexList > -1) {

    copy[targetIndexList] = match;

  }

  setData(copyList)

}

const handleReset = (id) => {

  const copy = [...data];

  const targetIndex = copy.findIndex(f => f.id === id);

  if (targetIndex > -1) {

    var raw = [];

    const options = data.filter(el => el.id === id)[0].options;

    for (let x of options) raw.push({ title: x.title, state: false})

    copy[targetIndex].options = raw;

  }

  setData(copy)

}

  return(

    <Containter>
     
      <Card />

      <Prote icon={Pro} />
           
     {data.map((v, i) => <Submenu {...v} onChange={handleChange} onReset={handleReset} />)}

    </Containter>

    );
}

export default App;
