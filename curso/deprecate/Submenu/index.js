import React from 'react';
import styled from 'styled-components';
import Options from './options.js';
import Footer from './footer';

const Containter = styled.div`
margin-top: 15px;
margin-left: 20px;
background-color: ${props => props.color};
border-radius: 25px;
//height: 200px;
width: 400px;
padding-top: 15px;
padding-bottom: 25px;
//opacity: 0.8;
`;


const App = (params) => { 

      /**
       * color: string;
       * options: string[];
       * 
       */

const handleChange = (value) => {
  if (typeof params.onChange === 'function') params.onChange(value, params.id);

} 

const handleReset = () => {
  if (typeof params.onReset === 'function') params.onReset(params.id);

} 

       return(
         
       <Containter color={params.color}>

       
       	
        {params.options.map((v, i) => <Options key={i} {...v} onChange={handleChange} />)}
       
       <Footer version={params.footer} onReset={handleReset} /* onReset onCancel onAcept*/ />

       </Containter>

		);
};

export default App;


