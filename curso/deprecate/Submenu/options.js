import React from 'react';
import styled from 'styled-components';
import Check from '../images/check-k.png';


const Containter = styled.div`
margin-left: 25px;
margin-top: 15px;
display: flex;
flex-direction: row;
justify-content: space-between;
margin-right: 25px;
align-items: center;
`;

const Title = styled.p`
margin: 0px;
color: white;
font-weight: 500;
`

export const Circulo = styled.div`
border-radius: 50%;
border: 1.8px solid white;
height: 35px;
width: 35px;
cursor: pointer;
background-color: ${props => props.visible ? 'white': 'tranparente'};
`;
export const Icon = styled.img`
height: 35px;
width: 35px;
object-fit: contain;
border-radius: 50%;
visibility: ${props => props.visible ? 'visible' : 'hidden'};
`;

const App = ({ title, state, onChange }) => {

const handleChange = () => {
     if (typeof onChange === 'function') onChange(title);
};
	 
	 return (
       
       <Containter>
       	
       	<Title>
       		{title}
       	</Title>
         
         <Circulo visible={state} onClick={handleChange} >
         	<Icon src={Check} visible={state}/>
         </Circulo>

       </Containter>

		);
};

export default App;


