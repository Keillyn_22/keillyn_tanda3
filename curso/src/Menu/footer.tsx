import styled from 'styled-components';
import Checkbox from './checkbox.tsx';
import Check from '../images/check-k.png';
import cancelar from '../images/cancel.png';
import reset from '../images/reset.png';

const Containter = styled.div`
display: flex;
justify-content: center;
align-items: center;
flex-direction: row;
`;

const ContainterV1 = styled.div`
display: flex;
justify-content: center;
align-items: center;
flex-direction: row;
margin-top: 15px;
padding-top: 25px;
padding-bottom: 25px;
border-Top: 1px solid #ffffff80;
`;

interface option {
	icon: string;
	onClick: () => void;
}

export interface params {
	enableReset: boolean;
	enableCancel: boolean;
};

const  App = (params: params): JSX.Element => {

	  const handleReset = () => {
      
        console.log("Vamos a resetear");

        if (typeof onReset === 'function') onReset() 
    };

     const handleOk = () => {
       
        console.log("Vamos a aceptar");

        if (typeof onReset === 'function') onAcept() 
    };
 
      const handleCancel = () => {
      
        console.log("Vamos a cancelar");

        if (typeof onReset === 'function') OnCancel() 
     };

    const data: option[] = [
    {icon: cancelar, onClick: handleCancel },
    {icon: reset, onClick: handleReset },
    {icon: Check, onClick: handleOk }
    ];

	return (
         <Containter>

           {data.map((v, i) => <Checkbox state={false} onClick={v.onClick} />)}  

         </Containter>

	);
};

export default App;